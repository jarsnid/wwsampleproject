$(document).ready(function () {
    //Client Side validation  (I got Django's default Server Side validation working before implementing this.)
    //I went with a jQuery plugin that I haven't used before in an attempt to make this faster.  It was written by a
    //member of the jQuery team.  Source:  http://jqueryvalidation.org/

    $("#course_form").validate({
        //Validation options:
        errorClass: "text-danger",
        //admittedly, this would be cleaner if it would use my fields required attribute.
        // Which, I could implement using context / template variables, but I will not due to time constraints.
        rules: {
            title: "required",
            description: "required",
            instructor: "required",
            duration: "required"
            //Issue: If I make course_art required, it can't find currently picked files. So you can't resubmit an
            // existing photo.  Thus, in the interest of time, I will rely on Server Side validation for this field.
            //course_art: "required"
        }
        //You can attach custom messages here. This is a very fast Client side validation implementation.
        //messages: {
        //    title: "The title of the course is required.",
        //    description: "We already asked you once... ",
        //    instructor: "Who is teaching the course?  No one? Really?",
        //    duration: "How long?",
        //    course_art: "You must have a logo for your course!  Branding is powerful!"
        //}
    });

    //taken from:  http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

     $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        console.log(numFiles);
        console.log(label);
         $("#file_name").text(label);
    });

});