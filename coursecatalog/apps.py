from django.apps import AppConfig


class CoursecatalogConfig(AppConfig):
    name = 'coursecatalog'
