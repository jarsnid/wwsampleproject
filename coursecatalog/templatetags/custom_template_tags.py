from django import template

register = template.Library()


@register.filter(name='format_duration_as_week')
def format_duration_as_week(duration_timedelta):
    """
    :param duration_timedelta: a duration model field (uses python's TimeDelta object)
    :return: A string that displays the number of weeks in a duration time delta object
    """

    #  *Well, this template filter isn't going to be used now that I've found get_foo_display*

    # Note, there is an implicit assumption that I am only dealing w/ timedelta objects that have full weeks (2 & 8)
    # (i.e., day values that are perfectly divisible by 7). This approach would have to be changed if it was going to
    # handle partial weeks (e.g., 4 days ).  Also, this could be done by adding a property function to the Course
    # model, but I implemented this first before remembering about properties on models.

    # Make sure that this is a time duration made up of full weeks (divisible by 7):
    if duration_timedelta.days % 7 == 0:
        return str(int(duration_timedelta.days / 7)) + " weeks"  # conversion to int to strip .0 from string.
    else:
        # Going to raise an error instead of just returning "" so that way there is no possibility of missing a bad
        # timedelta value (despite limiting the model values to the two choices of 2 and 8 weeks respectively).
        raise ValueError("This filter cannot handle TimeDelta objects with partial weeks.")

