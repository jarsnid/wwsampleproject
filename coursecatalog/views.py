from django.shortcuts import render
from .models import *
from .forms import CourseForm
from django.contrib import messages
# Create your views here.


def course_form(request, course_id=None):
    """
    Allows users to add a new course to the course catalog.
    Allows users to modify and update an existing course's details.
    :param course_id: The Pk or Id of the course object to be edited.
    :param request: The Django HttpRequest object
    :return: HttpResponse
    """

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        if course_id:
            # Updating an existing record so get the model instance to be updated
            course = Course.objects.get(pk=course_id)
            # Using _ to avoid shadowing outer scope form variable
            _course_form = CourseForm(request.POST, request.FILES, instance=course)

        else:
            # Creating a new record using blank form populated w/ the post data:
            _course_form = CourseForm(request.POST, request.FILES)

        # If the data is valid, save it and return success, else return failure
        if _course_form.is_valid():
            _course_form.save()
            messages.success(request, 'Course Saved Successfully')
        else:
            messages.error(request, 'Course Save Unsuccessful')

        context = {
            'course_id': course_id,
            'course_form': _course_form,
            'request': request
        }

        return render(request, 'coursecatalog/course_form.html', context)

    # if a GET or any other method, create a form based on if course_id was passed to the view:
    else:
        # Retrieving info on existing item
        if course_id:
            course = Course.objects.get(pk=course_id)
            _course_form = CourseForm(instance=course)

        # Creating new, blank course form
        else:
            _course_form = CourseForm()

        context = {
            'course_id': course_id,
            'course_form': _course_form,
            'request': request
        }

        return render(request, 'coursecatalog/course_form.html', context)


def list_courses(request):
    """
    Lists all current courses in the catalog in an easy to read fashion.
    :param request: The Django HttpRequest object
    :return: HttpResponse
    """

    # Retrieve course list from Course Models:
    course_list = Course.objects.all().order_by('pk')

    context = {'course_list': course_list}

    return render(request, 'coursecatalog/course_list.html', context)


def detail_course(request, course_id):
    """
    Displays all details (i.e., course model values) of a specific course in the catalog in an easy to read fashion.
    :param course_id:  The primary key of the Course to be displayed.
    :param request: The Django HttpRequest object
    :return: HttpResponse
    """

    # Retrieve specific course from Course Models:
    course = Course.objects.get(pk=course_id)

    context = {'course': course}

    return render(request, 'coursecatalog/course_details.html', context)


