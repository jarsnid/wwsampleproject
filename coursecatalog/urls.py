from django.conf.urls import url
from . import views

# The url() function is passed four arguments, two required: regex and view, and two optional: kwargs, and name

urlpatterns = [
    url(r'^$', views.list_courses, name='course_list'),
    url(r'^add/$', views.course_form, name='add_course'),
    url(r'^(?P<course_id>[0-9]+)/$', views.detail_course, name='detail_course'),
    url(r'^(?P<course_id>[0-9]+)/change/$', views.course_form, name='change_course'),
]