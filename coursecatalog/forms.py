from django.forms import ModelForm
from django import forms
from .models import Course


class CourseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)

        self.fields['title'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Name it something good'})
        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['instructor'].widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Course

        widgets = {
            'description': forms.Textarea(attrs={'rows': 2}),
            'duration': forms.RadioSelect(),
            # I could not figure out how to style the ImageField to be similar to the picture using forms.
            # I resorted to using a CSS and JavaScript solution.
            # 'course_art': forms.FileInput(attrs={'class': 'btn btn-primary btn-lg'}),
        }

        labels = {
            'title': 'Course Title',
            'description': 'Description',
            'instructor': 'Instructor',
            'duration': 'Course Duration',
            'course_art': 'Art',
        }

        help_texts = {
            'description': 'Provide some information about what students will learn.',
        }

        fields = ['title', 'description', 'instructor', 'duration', 'course_art']
