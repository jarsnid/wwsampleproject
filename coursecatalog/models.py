from django.db import models
import datetime
# Create your models here.


class Instructor(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Course(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=500)
    # One to one relationship with a single instructor--assuming there can only be one instructor per class.
    instructor = models.ForeignKey(Instructor)

    # Duration Field Options:
    TWOWEEKS = datetime.timedelta(weeks=2)
    EIGHTWEEKS = datetime.timedelta(weeks=8)
    DURATION_CHOICES = (
        (TWOWEEKS, '2 Weeks'),
        (EIGHTWEEKS, '8 Weeks'),
    )
    duration = models.DurationField(choices=DURATION_CHOICES, default=TWOWEEKS)
    course_art = models.ImageField(upload_to='uploads/')  # file will be uploaded to MEDIA_ROOT/uploads

    def __str__(self):
        # Sample string representation:   title: description (instructor)
        return self.title + ": " + self.description + " (" + self.instructor.name + ")"
