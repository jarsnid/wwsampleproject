from django.contrib import admin
from .models import *

# Register your models here.

# Going to add my own Model Admins here so that I can see PKs and table layout instead of what Model's __str___ method
# returns.


class InstructorAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name')


class CourseAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', 'description', 'instructor', 'duration', 'course_art')


admin.site.register(Instructor, InstructorAdmin)
admin.site.register(Course, CourseAdmin)
