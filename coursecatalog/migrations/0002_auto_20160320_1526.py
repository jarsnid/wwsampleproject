# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-20 19:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coursecatalog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='duration',
            field=models.DurationField(choices=[(datetime.timedelta(14), '2 Weeks'), (datetime.timedelta(56), '8 Weeks')], default=datetime.timedelta(14)),
        ),
    ]
